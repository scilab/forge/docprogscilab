// Copyright (C) 2010 - Michael Baudin

// How to compute a^p ?
// What is the complexity of the algorithm ?

// Use a^p = a * a^(p-1) until p = 1
function [ y , n ] = power1 ( a , p )
  y = 1
  n = 0
  while ( p > 0 )
    y = y * a
    p = p - 1
    n = n + 1 
  end
endfunction

disp([power1(2,2) 2^2])
disp([power1(2,3) 2^3])
disp([power1(2,4) 2^4])
disp([power1(2,5) 2^5])

// What is is complexity with respect to p?
// This is linear: 2 + 3b
for p = logspace(0,4,20)
  [ y , n ] = power1(2,p);
  disp([p n])
  plot ( p , n , "bo" )
end


// How to vectorize this function ?
function y = power2 ( a , p )
  if ( size(a) <> size(p) ) then
    error ("power2: The input arguments a and p must have the same size" )
  end
  y = ones(a)
  while ( %t )
    i = find ( p > 0 )
    if ( i == [] ) then
      break
    end
    y(i) = a(i) .* y(i)
    p(i) = p(i) - 1
  end
endfunction

a = 2*ones(10,1);
p = (1:10)';
y = power2(a,p);
[a p y a.^p]

// What is is complexity with respect to the size of the array?
// This is quadratic ?
for n = logspace(0,4,20)
  a = 2*ones(n,1);
  p = (1:n)';
  tt = timer();
  y = power2(a,p);
  tt = timer();
  disp([n tt])
  plot ( n , tt , "bo" )
end

// How to make it faster ?
// If p is even, a^p = (a*a)^(p/2)
// If p is odd, a^p = a * a^(p-1)
// This reduces the problem by half, when p is even.
// If p is odd, then, next time it will be even.
function [ y , n ] = power3 ( a , p )
  n = 0
  y = 1
  while ( p > 0 )
    r = pmodulo ( p , 2 )
    if ( r == 1 ) then
      y = a * y
      p = p - 1
      n = n + 1
    end
    a = a * a
    p = p / 2
    n = n + 1
  end
endfunction

disp([power3(2,2) 2^2])
disp([power3(2,3) 2^3])
disp([power3(2,4) 2^4])
disp([power3(2,5) 2^5])
disp([power3(2,6) 2^6])
disp([power3(2,7) 2^7])
disp([power3(2,8) 2^8])
disp([power3(2,9) 2^9])

// Check systematically
for p = 1 : 20
  disp([power3(2,p) 2^p])
end
for p = 1 : 20
  disp([power3(3,p) 3^p])
end

// What is is complexity with respect to p?
// p even: t(p) = 6 + t(p/2)
// p od: t(p) = 6 + t(p-1) = 6 + t((p-1)/2)
// With abuse: 
// t(p) = 6 + t(p/2)
// t(p) = 6 + 6 + t(p/4) = 12 + t(p/4)
// In general: t(p) = 12k + t(p/2^k)
// We are done when p/2^k = 1 => k = log2(p)
for x = logspace(0,4,20)
  p = int(x);
  [ y , n ] = power3(2,p);
  disp([p n])
  plot ( p , n , "bo" )
end

// How to vectorize this function ?
function y = power4 ( a , p )
  if ( size(a) <> size(p) ) then
    error ("power2: The input arguments a and p must have the same size" )
  end
  y = ones(a)
  while ( %t )
    i = find ( p > 0 )
    if ( i == [] ) then
      break
    end
    r(i) = pmodulo ( p(i) , 2 )
    ic = find ( p == 0 )
    r(ic) = -1
    //
    j = find ( r == 1 )
    y(j) = a(j) .* y(j)
    p(j) = p(j) - 1
    //
    j = find ( r == 0 ) 
    a(j) = a(j) .* a(j)
    p(j) = p(j) ./ 2
  end
endfunction

a = 2*ones(20,1);
p = (1:20)';
y = power4(a,p);
[a p y a.^p]

// A variant, with odd powers processed meanwhile.
// 2009 - Vladimir Sergievskiy
function p = fast_pow ( A , p )
  p = eye(A);
  while ( p>0 )
    if ( modulo(p,2) == 1) then
      p = p * A;
    end
    p = int(p/2);
    A = A * A;
  end
endfunction

A = testmatrix("frk",5)

// Test with odd power
A^7
fast_pow(A,7)

// Test with even power
A^6
fast_pow(A,6)

// Compare perfs
p=rand(500,500);
 
p=p./(sum(p,'r')*ones(500,1));
 
tic,C=p^10;,toc
tic,D=fast_pow(p,10);,toc
norm(C-D)/norm(C)

// Test other cases
A = ones(5,5)
A^1.5

// Quadratic complexity
function [ y , n ] = matmul ( A , x )
  nrows = size(A,"r")
  ncols = size(A,"c")
  y = zeros(nrows,1)
  n = 0
  for i = 1 : nrows
    for j = 1 : ncols
      y(i) = y(i) + A(i,j) * x(j)
      n = n + 1
    end
  end
endfunction

A = ones(5,4)
x = ones(4,1)
[y,n] = matmul ( A , x );

// What is the complexity in time ?
for n = logspace(0,2,20)
  A = ones(n,n);
  x = ones(n,1);
  tt = timer();
  y = matmul ( A , x );
  tt = timer();
  disp([n tt])
  plot ( n , tt , "bo" )
end

// What is the complexity in number of operations ?
for nrows = int(logspace(0,2,20))
  A = ones(nrows,nrows);
  x = ones(nrows,1);
  tt = timer();
  [y,n] = matmul ( A , x );
  tt = timer();
  disp([nrows n])
  plot ( nrows , n , "bo" )
end

// A variant, with odd powers processed meanwhile.
// 2009 - Vladimir Sergievskiy
// 2010 - Michael Baudin
function B = fastpow ( A , p )
  B = eye(A)
  while ( p>0 )
    r = modulo(p,2)
    if ( r == 1) then
      B = A * B
      p = p - 1
    end
    A = A * A
    p = p/2
  end
endfunction
A = testmatrix("frk",5)

// Test with odd power
A^5
fastpow(A,5)

// Test with even power
A^6
fastpow(A,6)

// Test with odd power
A^7
fastpow(A,7)

A = (3  - 2 * %i ) * testmatrix("frk",5)
A^6
fastpow(A,6)

