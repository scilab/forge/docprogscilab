// Copyright (C) 2009 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin

//
// A simplified demo of the Kronecker product, used to vectorize 
// a function producing combinations.

// This script requires the scibench module.
if ( ~atomsIsInstalled("scibench") ) then
    atomsInstall("scibench")
    atomsLoad("scibench")
end

// For the report, we use the benchfun function.
exec benchfun.sci;

function c = combinatevectorsSlow ( x , y )
   // Returns all combinations of the two row vectors x and y
   cx=size(x,"c")
   cy=size(y,"c")
   c=zeros(2,cx*cy)
   k = 1
   for ix = 1 : cx
     for iy = 1 : cy
       c(1:2,k) = [x(ix);y(iy)]
       k = k + 1
     end
   end
endfunction
function c = combinatevectorsKron ( x , y )
   // Returns all combinations of the two row vectors x and y
   cx=size(x,"c")
   cy=size(y,"c")
   c(1,:) = x .*. ones(1,cy)
   c(2,:) = ones(1,cx) .*. y
endfunction
function c = combinatevectorsExtr ( x , y )
   // Returns all combinations of the two row vectors x and y
   cx=size(x,"c")
   cy=size(y,"c")
   //
   i1 = 1:cx
   j1 = i1(ones(cy,1),:)
   k1 = matrix(j1,1,cx*cy)
   //
   i2 = 1:cy
   j2 = i2(ones(cx,1),:)
   k2 = matrix(j2',1,cx*cy)
   //
   c(1,:)=x(:,k1)
   c(2,:)=y(:,k2)
endfunction
//
// Direct test of Kronecker product
[10 11 12] .*. [1 1 1]
[1 1 1] .*. [13 14 15]
//
// Test with matrix extraction
x = [10 11 12];
i1 = 1:3
j1 = i1([1;1;1],:)
k1 = matrix(j1,1,9)
x(:,k1)
//
y = [13 14 15];
i2 = 1:3
j2 = i2([1;1;1],:)
k2 = matrix(j2',1,9)
y(:,k2)
//
c = [
x(:,k1)
y(:,k2)
]
//
// Check both implementations
x = [10 11 12];
y = [13 14 15];
c = combinatevectorsSlow ( x , y )
c = combinatevectorsKron ( x , y )
c = combinatevectorsExtr ( x , y )
//
// Test for large x and y
n = 300;
x=(1:n);
y=(1:n);
benchfun("Slow",combinatevectorsSlow,list(x,y),1,10);
benchfun("Kron.",combinatevectorsKron,list(x,y),1,10);
benchfun("Extr.",combinatevectorsExtr,list(x,y),1,10);

// See timing increase
function t = combSlown ( n )
  n = floor(sqrt(n))
  x=(1:n);
  y=(1:n);
  tic()
  c = combinatevectorsSlow ( x , y )
  t = toc()
endfunction  
function t = combKronn ( n )
  n = floor(sqrt(n))
  x=(1:n);
  y=(1:n);
  tic()
  c = combinatevectorsKron ( x , y )
  t = toc()
endfunction  
function t = combExtrn ( n )
  n = floor(sqrt(n))
  x=(1:n);
  y=(1:n);
  tic()
  c = combinatevectorsExtr ( x , y )
  t = toc()
endfunction  
perftable = scibench_dynbenchfun ( combSlown , %t, %t , 0.1 , 3 , 1.2 );
perftable = scibench_dynbenchfun ( combKronn , %t, %t , 0.1 , 3 , 1.2 );
perftable = scibench_dynbenchfun ( combExtrn , %t, %t , 0.1 , 3 , 1.2 );

//
// Test with matrix extraction
x = [1 2 3]
y = [4 5 6]
combinatevectorsSlow ( x , y )
combinatevectorsKron ( x , y )
combinatevectorsExtr ( x , y )

//
// More complicated test
x = [1,1,1,2,2,2;  
4,5,6,4,5,6]   
y = [7 8 9]
combinatevectorsSlow ( x , y )
combinatevectorsKron ( x , y )
combinatevectorsExtr ( x , y )

