// Copyright (C) 2008 - 2010 - Michael Baudin


// linalg_gausspivotal --
//   Returns the solution of Ax = b
//   with a Gauss elimination and row exchanges.
//   This algorithm might be sensitive to inaccuracies if A near singular.
//   There is no solution is A is singular.
//   If A is not ill-conditionned, the algorithm is accurate.
//   Uses an effective algorithm with vectorization.
//   TODO : add b at the end of A and performs the forward-backward loop in the 
//   same loop as factorization.
function x = gausspivotal ( A , b ) 
  n = size(A,"r")
  // Initialize permutation
  P=zeros(1,n-1)
  // Perform Gauss transforms
  for k=1:n-1
    // Search pivot
    [abspivot,murel]=max(abs(A(k:n,k)))
    // Shift mu, because max returns with respect to (k:n,k)
    mu = murel + k - 1
    // Swap lines k and mu from columns k to n
    A([k mu],k:n) = A([mu k],k:n)
    P(k) = mu
    // Perform transform for lines k+1 to n
    A(k+1:n,k) = A(k+1:n,k)/ A(k,k)
    A(k+1:n,k+1:n) = A(k+1:n,k+1:n) - A(k+1:n,k) * A(k,k+1:n)
  end
  // Perform forward substitution
  for k=1:n-1
    // Swap b(k) and b(P(k))
    mu = P(k)
    b([k mu]) = b([mu k])
    // Substitution
    b(k+1:n) = b(k+1:n) - b(k) * A(k+1:n,k)
  end
  // Perform backward substitution
  for k=n:-1:1
    b(k) = (b(k) - A(k,k+1:n) * b(k+1:n)) / A(k,k)
  end
  x = b
endfunction


