// Copyright (C) 2010 - DIGITEO - Michael Baudin

function stacksizehuman ( varargin )
  // Prints a human-readable state of the stack.
  //
  // Calling Sequence
  //   stacksizehuman ( )
  //   stacksizehuman ( unitsystem )
  //
  // Parameters
  //   unitsystem : a 1-by-1 matrix of booleans, unitsystem=%t for decimal, unitsystem=%f for binary (Default unitsystem=%f, i.e. decimal)
  //   mstr : a 1-by-1 matrix of strings, the memory in the OS
  //
  // Description
  //   Consider the number of doubles in the current stack 
  //   and returns a string corresponding to the memory 
  //   in the Operating System.
  //
  //   If unitsystem=%t (decimal), the number of bytes in one KB is 1000.
  //   If unitsystem=%f (binary), the number of bytes in one KB is 1024.
  //
  // Example
  //   stacksizehuman ( )
  //

  [lhs,rhs] = argn()
  if ( and(rhs<>[0 1]) ) then
    noerrmsg = sprintf(gettext("%s: Unexpected number of arguments : %d provided while %d to %d are expected."),..
      "stacksize2memory",rhs,0,1);
    error(noerrmsg)
  end
  if ( rhs < 1 ) then
    unitsystem = %t
  else
    unitsystem = varargin(1)
  end
  //
  // Proceed...
  sz = stacksize()
  m = sz .* 64 ./ 8
  mstr(1) = humanreadablememory ( m(1) , unitsystem )
  mstr(2) = humanreadablememory ( m(2) , unitsystem )
  mprintf("Stack Size Total: %s, Used: %s\n",mstr(1),mstr(2))
endfunction

