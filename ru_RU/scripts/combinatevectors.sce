// Copyright (C) 2009 - Michael Baudin

// combinatevectors --
//   Returns the all the combinations of the given vectors.
// Arguments
//   a, b, c, ... : row vectors which all have the same number of elements
// Example #1
//     x = [1 2 3];
//     y = [4 5 6];
//     combinatevectors ( x , y )
//     expected = [
//       1 1 1 2 2 2 3 3 3. 
//       4 5 6 4 5 6 4 5 6. 
//     ];
// Example #2
//     x = [1 2 3];
//     y = [4 5 6];
//     z = [7 8 9];
//     combinatevectors ( x , y , z )
//     expected = [
//       1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 3 3 3 3 3 3 3 3 3;  
//       4 4 4 5 5 5 6 6 6 4 4 4 5 5 5 6 6 6 4 4 4 5 5 5 6 6 6;   
//       7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9
//     ];
//    
// Example #3
//     x = [1 2;3 4];
//     y = [5 6;7 8];
//     combinatevectors ( x , y )
//     expected = [
//       1 1 2 2
//       3 3 4 4 
//       5 6 5 6 
//       7 8 7 8 
//     ];
//
// Authors
//   Yann Collette
//   Pierre Lando
//   Michael Baudin
//
function R = combinatevectors ( varargin )
 [lhs,rhs]=argn()
 // Check that all arguments are either row vectors or column vectors
 areallrows = %t
 areallcolumns = %t
 for i = 1:rhs
   X = varargin(i)
   if (size(X,1) <> 1) then
     areallrows = %f
   end
   if (size(X,2) <> 1) then
     areallcolumns = %f
   end
 end
 if (~areallrows) & (~areallcolumns) then
   error ( msprintf ( gettext ( "%s: All arguments must be either row or column vectors."),"combinatevectors"))
 end
 if rhs == 1 then
   R = X
   return
 end
 if areallrows then
   R = combinatevectors_2args ( varargin(1) , varargin(2) )
   for i = 3:rhs
     R = combinatevectors_2args ( R , varargin(i) )
   end
 else
   R = combinatevectors_2args ( varargin(1).' , varargin(2).' )
   for i = 3:rhs
     R = combinatevectors_2args ( R , varargin(i).' )
   end
   R = R.'
 end
endfunction
function R = combinatevectors_2args ( X , Y )
   // Returns all combinations of the two row vectors X and Y
   cx=size(X,"c")
   cy=size(Y,"c")
   R=[X .*. ones(1,cy); ones(1,cx) .*. Y]
endfunction
//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
 if expected==0.0 then
   shift = norm(computed-expected);
 else
   shift = norm(computed-expected)/norm(expected);
 end
 if shift < epsilon then
   flag = 1;
 else
   flag = 0;
 end
 if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
 if computed==expected then
   flag = 1;
 else
   flag = 0;
 end
 if flag <> 1 then pause,end
endfunction
//
// Test 2 row vectors
x = [1 2 3];
y = [4 5 6];
computed = combinatevectors ( x , y );
expected = [
1 1 1 2 2 2 3 3 3; 
4 5 6 4 5 6 4 5 6
];
assert_equal ( computed , expected );
//
// Test 3 row vectors
x = [1 2 3];
y = [4 5 6];
z = [7 8 9];
computed = combinatevectors ( x , y , z );
expected = [
1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 3 3 3 3 3 3 3 3 3;  
4 4 4 5 5 5 6 6 6 4 4 4 5 5 5 6 6 6 4 4 4 5 5 5 6 6 6;   
7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9
];
assert_equal ( computed , expected );
x = [1 2;3 4];
y = [5 6;7 8];
cmd = "computed = combinatevectors ( x , y );";
execstr(cmd,"errcatch");
computed = lasterror();
expected = "combinatevectors: All arguments must be either row or column vectors.";
assert_equal ( computed , expected );
//
// Test 2 column vectors
x = [
1
2
3
];
y = [
4
5
6
];
computed = combinatevectors ( x , y );
expected = [
1 4;   
1 5;   
1 6;   
2 4;   
2 5;   
2 6;   
3 4;   
3 5;   
3 6
];
assert_equal ( computed , expected );
//
// Test 3 column vectors
x = [
1
2
3
];
y = [
4
5
6
];
z = [
7
8
9
];
computed = combinatevectors ( x , y , z );
expected = [
1 4 7;  
1 4 8;   
1 4 9;   
1 5 7;   
1 5 8;   
1 5 9;   
1 6 7;   
1 6 8;   
1 6 9;   
2 4 7;   
2 4 8;   
2 4 9;   
2 5 7;   
2 5 8;   
2 5 9;   
2 6 7;   
2 6 8;   
2 6 9;   
3 4 7;   
3 4 8;   
3 4 9;   
3 5 7;   
3 5 8;   
3 5 9;   
3 6 7;   
3 6 8;   
3 6 9
];
assert_equal ( computed  , expected );

