// Copyright (C) 2010 - Michael Baudin

/////////////////////////////////////////////////////
// A demo of the execstr function when it is used to 
// hide a set of functions.

function [name,x] = dataset1 ( info , n )
  name = []
  x = []
  if ( info == 1 ) then
    name = "Linear from 0 to 1"
  end
  if ( info == 2 ) then
    x = linspace(0,1,n)
  end
endfunction

function [name,x] = dataset2 ( info , n )
  name = []
  x = []
  if ( info == 1 ) then
    name = "Logarithmic from 1 to 10"
  end
  if ( info == 2 ) then
    x = logspace(1,10,n)
  end
endfunction

n = 5;
[name,x] = dataset1 ( 1 , n )
[name,x] = dataset1 ( 2 , n )
[name,x] = dataset2 ( 1 , n )
[name,x] = dataset2 ( 2 , n )

// The API
// name = dataset_getname(i) : returns the name of dataset #i
// x = dataset_getx(i,n) : returns the x of dataset #i

function name = dataset_getname(i)
  if ( i<1 | i>2 ) then
    error(msprintf("Unknown dataset #%d",i))
  end
  instr=msprintf("[name,x] =dataset%d(1,[])",i)
  ierr = execstr(instr,"errcatch")
  if ( ierr <> 0 ) then
    error(lasterror())
  end
endfunction
function x = dataset_getx(i,n)
  if ( i<1 | i>2 ) then
    error(msprintf("Unknown dataset #%d",i))
  end
  instr=msprintf("[name,x] =dataset%d(2,n)",i)
  ierr = execstr(instr,"errcatch")
  if ( ierr <> 0 ) then
    error(lasterror())
  end
endfunction

n = 5;
name = dataset_getname(1)
x = dataset_getx(1,n)
name = dataset_getname(2)
x = dataset_getx(2,n)


