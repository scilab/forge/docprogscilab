// Copyright (C) 2009 - Michael Baudin

//
// A simplified demo of the Kronecker product, used to vectorize 
// a function producing combinations.

function c = combinatevectorsSlow ( x , y )
   // Returns all combinations of the two row vectors x and y
   cx=size(x,"c")
   cy=size(y,"c")
   c=zeros(2,cx*cy)
   k = 1
   for ix = 1 : cx
     for iy = 1 : cy
       c(1:2,k) = [x(ix);y(iy)]
       k = k + 1
     end
   end
endfunction
function c = combinatevectorsFast ( x , y )
   // Returns all combinations of the two row vectors x and y
   cx=size(x,"c")
   cy=size(y,"c")
   c=[
     x .*. ones(1,cy)
     ones(1,cx) .*. y
   ]
endfunction
//
// Direct test of Kronnecker product
x = [1 2 3];
y = [4 5 6];
cx=size(x,"c");
cy=size(y,"c");
c=[
  x .*. ones(1,cy); 
  ones(1,cx) .*. y
];
//
// Check both implementations
x = [1 2 3]
y = [4 5 6]
c = combinatevectorsSlow ( x , y )
c = combinatevectorsFast ( x , y )
//
// Test for large x and y
n = 300;
x=(1:n);
y=(1:n);
benchfun("Slow",combinatevectorsSlow,list(x,y),1,10);
benchfun("Fast",combinatevectorsFast,list(x,y),1,10);

// See timing increase
function c = combSlown ( n )
  x=(1:n);
  y=(1:n);
  c = combinatevectorsSlow ( x , y )
endfunction  
function c = combFastn ( n )
  x=(1:n);
  y=(1:n);
  c = combinatevectorsFast ( x , y )
endfunction  
perftable = scibench_dynbenchfun ( %t , %t , 0.1 , 8 , 1.2 , combFastn , list() , 1 );
perftable = scibench_dynbenchfun ( %t , %t , 0.1 , 8 , 1.2 , combSlown , list() , 1 );

