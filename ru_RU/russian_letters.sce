// Copyright (C) Кротер С.В. 2011
//
// Программа замены TeX-овской транслитерации русских букв на русские буквы в индексе
//    Предполагается, что текущая папка docprogscilab/ru_RU
//
// ============== Алгоритм использования этой программы ===============
// 1) Скомпилировать документ для получения файла progscilab-so.idx
// 2) Убедиться, что текущая папка Сайлаба та, где расположен progscilab-so.idx, если нет, то перейти туда
// 3) Запустить russian_letters.sce
// 4) Выполнить makeindex progscilab-so.idx (будет получент progscilab-so.ind)
// 5) Повторно запустить компиляцию документа, чтобы применились обновления индекса

stacksize("max")

try
    fid_1 = mopen("progscilab-so.idx","r");
catch
    messagebox(["Не удалось открыть файл progscilab-so.idx";..
                "Убедись, что ты в нужном месте."
                "Если ты в нужном месте, то надо заново скомпилировать документ"],'Ошибка',"error")
end

letters= ["\IeC {\CYRA }","А";..
          "\IeC {\CYRB }","Б";..
          "\IeC {\CYRV }","В";..
          "\IeC {\CYRG }","Г";..
          "\IeC {\CYRD }","Д";..
          "\IeC {\CYRE }","Е";..
          "\IeC {\CYRYO }","Ё";..
          "\IeC {\CYRZH }","Ж";..
          "\IeC {\CYRZ }","З";..
          "\IeC {\CYRI }","И";..
          "\IeC {\CYRISHRT }","Й";..
          "\IeC {\CYRK }","К";..
          "\IeC {\CYRL }","Л";..
          "\IeC {\CYRM }","М";..
          "\IeC {\CYRN }","Н";..
          "\IeC {\CYRO }","О";..
          "\IeC {\CYRP }","П";..
          "\IeC {\CYRR }","Р";..
          "\IeC {\CYRS }","С";..
          "\IeC {\CYRT }","Т";..
          "\IeC {\CYRU }","У";..
          "\IeC {\CYRF }","Ф";..
          "\IeC {\CYRH }","Х";..
          "\IeC {\CYRC }","Ц";..
          "\IeC {\CYRCH }","Ч";..
          "\IeC {\CYRSH }","Ш";..
          "\IeC {\CYRSHCH }","Щ";..
          "\IeC {\CYRHRDSN }","Ъ";..
          "\IeC {\CYRERY }","Ы";..
          "\IeC {\CYRSFTSN }","Ь";..
          "\IeC {\CYREREV }","Э";..
          "\IeC {\CYRYU }","Ю";..
          "\IeC {\CYRYA }","Я";..
          "\IeC {\cyra }","а";..
          "\IeC {\cyrb }","б";..
          "\IeC {\cyrv }","в";..
          "\IeC {\cyrg }","г";..
          "\IeC {\cyrd }","д";..
          "\IeC {\cyre }","е";..
          "\IeC {\cyryo }","ё";..
          "\IeC {\cyrzh }","ж";..
          "\IeC {\cyrz }","з";..
          "\IeC {\cyri }","и";..
          "\IeC {\cyrishrt }","й";..
          "\IeC {\cyrk }","к";..
          "\IeC {\cyrl }","л";..
          "\IeC {\cyrm }","м";..
          "\IeC {\cyrn }","н";..
          "\IeC {\cyro }","о";..
          "\IeC {\cyrp }","п";..
          "\IeC {\cyrr }","р";..
          "\IeC {\cyrs }","с";..
          "\IeC {\cyrt }","т";..
          "\IeC {\cyru }","у";..
          "\IeC {\cyrf }","ф";..
          "\IeC {\cyrh }","х";..
          "\IeC {\cyrc }","ц";..
          "\IeC {\cyrch }","ч";..
          "\IeC {\cyrsh }","ш";..
          "\IeC {\cyrshch }","щ";..
          "\IeC {\cyrhrdsn }","ъ";..
          "\IeC {\cyrery }","ы";..
          "\IeC {\cyrsftsn }","ь";..
          "\IeC {\cyrerev }","э";..
          "\IeC {\cyryu }","ю";..
          "\IeC {\cyrya }","я"];
fid = mopen('result.idx','w')

while ~meof(fid_1)
    str = mgetl(fid_1,1);
    for i = 1:66
        str = strsubst(str,letters(i,1),letters(i,2))
    end
    if isempty(str) then
        str = " ";
    end
    mputl(str,fid)
end

mclose(fid_1);
mclose(fid)

movefile("result.idx","progscilab-so.idx")